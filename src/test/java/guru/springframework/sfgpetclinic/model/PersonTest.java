package guru.springframework.sfgpetclinic.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@Tag("model")
class PersonTest {
    @Test
    void groupAssertions() {
        //given
        Person person = new Person(1L, "Joe", "Buck");
        //then
        assertEquals("Joe",person.getFirstName());
        assertEquals("Buck",person.getLastName());
    }

    @Test
    void groupAssertAll() {
        //given
        Person person = new Person(1L, "Joe", "Buck");
        //then
        assertAll(
                "test prop set",
                () -> assertEquals("Joe",person.getFirstName()),
                () -> assertEquals("Buck", person.getLastName())
        );
    }

    @Test
    void groupAssertAllWithMsg() {
        //given
        Person person = new Person(1L, "Joe", "Buck");
        //then
        assertAll(
                "test prop set",
                () -> assertEquals("Joe",person.getFirstName(),"first name failed(appear only when test failed)"),
                () -> assertEquals("Buck",person.getLastName(),"last name failed")
        );
    }


}