package guru.springframework.sfgpetclinic.model;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;


@Tag("model")
class OwnerTest {

    @Test
    void dependentAssertions() {
        //Pet pet=new Pet(1L,"mocha","dog","","");
        Owner owner = new Owner(1L, "sharon", "chen");
        owner.setCity("new york");
        owner.setTelephone("123123123");
        assertAll("Properties test",
                () -> assertAll("Person properties",
                        () -> assertEquals("sharon", owner.getFirstName(),"first name did not match"),
                        () -> assertEquals("chen", owner.getLastName(),"last  name did not match")
                ),
                () -> assertAll("Owner properties",
                        () -> assertEquals("new york", owner.getCity(),"city name did not match"),
                        () -> assertEquals("123123123", owner.getTelephone(),"telephone number did not match")
                )
        );


//test hamcret
        assertThat(owner.getCity(),is("new york"));
    }

    @Test
    void testGetPet() {
    }
}