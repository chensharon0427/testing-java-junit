package guru.springframework.sfgpetclinic.services.map;

import guru.springframework.sfgpetclinic.services.PetService;
import guru.springframework.sfgpetclinic.services.PetTypeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;



class OwnerMapServiceTest {
    PetService petService;
    PetTypeService petTypeService;
    OwnerMapService ownerMapService;
    @BeforeEach
    void setUp() {
        petService=new PetMapService();
        petTypeService=new PetTypeMapService();
        ownerMapService=new OwnerMapService(petTypeService,petService);
    }

    @Test
    void findAll() {
        int size = ownerMapService.findAll().size();
        assertThat(size).isZero();
    }
//next step: add nested test case
    @Test
    void findById() {
    }

    @Test
    void save() {
    }

    @Test
    void delete() {
    }

    @Test
    void deleteById() {
    }

    @Test
    void findByLastName() {
    }

    @Test
    void findAllByLastNameLike() {
    }
}