package guru.springframework.sfgpetclinic.controllers;

import guru.springframework.fauxspring.ModelMapImpl;
import guru.springframework.sfgpetclinic.fauxspring.Model;
import guru.springframework.sfgpetclinic.model.Speciality;
import guru.springframework.sfgpetclinic.model.Vet;
import guru.springframework.sfgpetclinic.services.SpecialtyService;
import guru.springframework.sfgpetclinic.services.VetService;
import guru.springframework.sfgpetclinic.services.map.SpecialityMapService;
import guru.springframework.sfgpetclinic.services.map.VetMapService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@Tag("controller")
class VetControllerTest {

    VetService vetService;
    SpecialtyService specialtyService;

    VetController vetController;

    @BeforeEach
    void setUp() {
        specialtyService = new SpecialityMapService();
        vetService = new VetMapService(specialtyService);

        vetController = new VetController(vetService);
        Vet vet1 = new Vet(1L, "joe", "buck", null); //Set<Speciality>
        Vet vet2 = new Vet(2L, "jimmy", "buck", null); //Set<Speciality>

        vetService.save(vet1);
        vetService.save(vet2);
    }

    @Test
    void testListVets() {
        Model model = new ModelMapImpl();//newly add modelmapImpl
        String view = vetController.listVets(model);
        assertThat("vets/index").isEqualTo(view);
        Set vetsAttributes = (Set) ((ModelMapImpl) model).getMap().get("vets");
        assertThat(vetsAttributes.size()).isEqualTo(2);
    }
}