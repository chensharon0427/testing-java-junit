package guru.springframework.sfgpetclinic.controllers;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.EnabledOnJre;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.JRE;
import org.junit.jupiter.api.condition.OS;

import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;


@Tag("controller")
class IndexControllerTest {

    IndexController controller;

    @BeforeEach
    void setUp() {
        controller = new IndexController();
    }

    @DisplayName("Index test case show here with display name annotation")
    @Test
    void index() {
        assertEquals("index", controller.index(), "error msg only appear when assert not passed");
    }

    @DisplayName("test exceptions")
    @Test
    void oopsHandler() {
//        assertTrue(controller.oupsHandler().equals("notimplemented"), () -> "lambda expression for error msg int test, " +
//                "it is expensive");
        //expect will throw exception
        assertThrows(ValueNotFoundException.class,()->controller.oopsHandler(),"exception does not match");
    }

    @Disabled(value="diabled timeout")
    @Test
    void testTimeOut() {

        assertTimeout(Duration.ofMillis(100), () -> {
            Thread.sleep(5000);

            System.out.println("I got here");
        });
    }

    @Disabled(value="disabled timeout preempt")
    @Test
    void testTimeOutPrempt() {
// 断言超时，如果在100毫秒内任务没有执行完毕，会立即返回断言失败，不会等到5000毫秒后
        assertTimeoutPreemptively(Duration.ofMillis(100), () -> {
            Thread.sleep(5000);
            System.out.println("I got here");
        });
    }

    @Test
    void testAssumptionTrue() {
//get env var, even it's not true, it does not need to fail and stop the whole test class
        assumeTrue("GURU".equalsIgnoreCase(System.getenv("GURU_RUNTIME")));
    }

    @Test
    void testAssumptionIsTrue() {
//get env var, even it's not true, it does not need to fail and stop the whole test class
        assumeTrue("GURU".equalsIgnoreCase("GURU"));
    }

    @EnabledOnOs(OS.MAC)
    @Test
    void testMeOnMacOS() {
    }

    @EnabledOnOs(OS.WINDOWS)
    @Test
    void testMeOnWindows() {
    }

    @EnabledOnJre(JRE.JAVA_8)
    @Test
    void testMeOnJava8() {
    }

    @EnabledOnJre(JRE.JAVA_11)
    @Test
    void testMeOnJava11() {
    }

    @Test
    void testAssertJ() {
//        assertThat("aaa").contains("a");
        assertThat(controller.index()).isEqualTo("index");
    }
}